﻿using FanControl.Plugins;

using GigaFan.MyDecompiledEnvironmentControl;

namespace FanControl.AltGigaPlugin
{
    internal class GigaTachSensor : IPluginSensor, IHoldsMyFanControlRef
    {
        private readonly int fanIndex;
        private MyFanControl fc;

        public string Id => fanIndex.ToString();

        public string Name { get; set; }

        public float? Value { get; set; }

        public GigaTachSensor(MyFanControl fc, int fanIndex)
        {
            this.fc = fc;
            this.fanIndex = fanIndex;
        }

        public void Update()
        {
            if (fc != null)
            {
                fc.GetFanSpeed(fanIndex, out float speed);
                Value = speed;
            }
        }

        public void RelinquishMyFanControlReference()
        {
            fc = null;
        }
    }
}
