﻿using FanControl.Plugins;

using GigaFan.MyDecompiledEnvironmentControl;

using System;

namespace FanControl.AltGigaPlugin
{
    internal class GigaControlSensor : IPluginControlSensor, IHoldsMyFanControlRef
    {
        private readonly int fanIndex;
        private MyFanControl fc;

        public string Id => fanIndex.ToString();

        public string Name { get; set; }

        public float? Value { get; set; }

        public GigaControlSensor(MyFanControl fc, int fanIndex)
        {
            this.fc = fc;
            this.fanIndex = fanIndex;
        }

        public void Reset()
        {
        }

        public void Set(float val)
        {
            fc?.SetFanSpeedFixedMode(fanIndex, f2b(val));
            Value = val;
        }

        private static byte f2b(float val)
        {
            //float targetMax = 100;
            //float result = val * targetMax;
            int r = (int)Math.Ceiling(val);
            return (byte)r;
        }

        public void Update()
        {
            //if (fc != null)
            //{
            //    fc.GetFanSpeed(fanIndex, out float speed);
            //    Value = speed;
            //}
        }

        public void RelinquishMyFanControlReference()
        {
            fc = null;
        }
    }
}
