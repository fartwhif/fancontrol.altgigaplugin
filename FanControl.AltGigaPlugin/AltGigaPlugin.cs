﻿using FanControl.Plugins;

using GigaFan;
using GigaFan.MyDecompiledEnvironmentControl;

using System;
using System.Collections.Generic;

namespace FanControl.AltGigaPlugin
{
    internal class AltGigaPlugin : IPlugin, IDisposable
    {
        public string Name => "AltGiga";

        private MyFanControl fc = null;
        private bool initialized;
        private bool disposed;

        private List<IHoldsMyFanControlRef> fcReferenceHolders = new List<IHoldsMyFanControlRef>();

        public void Close()
        {
            if (initialized)
            {
                for (int i = 0; i < fcReferenceHolders.Count; i++)
                {
                    fcReferenceHolders[i].RelinquishMyFanControlReference();
                }
                fcReferenceHolders.Clear();
                fcReferenceHolders = null;
                fc?.Dispose();
                fc = null;
                initialized = false;
            }
        }


        public void Initialize()
        {
            GigaFanHelper.GigaFanInit();
            fc = new MyFanControl();
            fcReferenceHolders = new List<IHoldsMyFanControlRef>();
            initialized = true;
        }

        public void Load(IPluginSensorsContainer _container)
        {
            if (initialized)
            {
                if (!fc.IsSupported)
                {
                    return;
                }

                List<GigaControlSensor> controls = new List<GigaControlSensor>();
                List<GigaTachSensor> tachs = new List<GigaTachSensor>();
                List<GigaTempSensor> temps = new List<GigaTempSensor>();
                for (int i = 0; i < fc.FanCount; i++)
                {
                    fc.GetFanControlDisplayName(i, out string fanControlDisplayName);
                    controls.Add(new GigaControlSensor(fc, i) { Name = fanControlDisplayName });

                    fc.GetFanDisplayName(i, out string fanDisplayName);
                    fc.GetFanSpeed(i, out float fanSpeed);
                    tachs.Add(new GigaTachSensor(fc, i) { Name = fanDisplayName, Value = fanSpeed });
                }

                for (int i = 0; i < fc.TemperatureCount; i++)
                {
                    fc.GetTemperatureDisplayName(i, out string tempDisplayName);
                    fc.GetTemperature(i, out float temp);
                    temps.Add(new GigaTempSensor(fc, i) { Name = tempDisplayName, Value = temp });
                }

                _container.FanSensors.AddRange(tachs);
                _container.ControlSensors.AddRange(controls);
                _container.TempSensors.AddRange(temps);

                fcReferenceHolders.AddRange(tachs);
                fcReferenceHolders.AddRange(controls);
                fcReferenceHolders.AddRange(temps);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                Close();
                disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~AltGigaPlugin()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
