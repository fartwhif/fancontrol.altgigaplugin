﻿namespace FanControl.AltGigaPlugin
{
    internal interface IHoldsMyFanControlRef
    {
        void RelinquishMyFanControlReference();
    }
}
