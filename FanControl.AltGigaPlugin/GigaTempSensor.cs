﻿using FanControl.Plugins;

using GigaFan.MyDecompiledEnvironmentControl;

namespace FanControl.AltGigaPlugin
{
    internal class GigaTempSensor : IPluginSensor, IHoldsMyFanControlRef
    {
        private readonly int sensorIndex;
        private MyFanControl fc;

        public string Id => sensorIndex.ToString();

        public string Name { get; set; }

        public float? Value { get; set; }

        public GigaTempSensor(MyFanControl fc, int sensorIndex)
        {
            this.fc = fc;
            this.sensorIndex = sensorIndex;
        }

        public void Update()
        {
            if (fc != null)
            {
                fc.GetTemperature(sensorIndex, out float temp);
                Value = temp;
            }
        }

        public void RelinquishMyFanControlReference()
        {
            fc = null;
        }
    }
}
