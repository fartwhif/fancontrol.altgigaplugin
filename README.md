# FanControl.AltGigaPlugin

Alternative Gigabyte Plugin for [FanControl](https://github.com/Rem0o/FanControl.Releases)

## Getting started

<ol>
<li>Import all submodules</li>
<li>Run prep.cmd</li>
<li>Compile FanControl.AltGigaPlugin</li>
<li>Copy output of .\fancontrol.altgigaplugin\FanControl.AltGigaPlugin\bin\Debug to your FanControl\Plugins folder</li>
</ol>

`FanControl.exe` must run in 32-bit mode.  While in 32-bit mode some built-in plugins no longer function, such as `NvApiWrapper` and/or `Nvidia curve overide`.

## Force FanControl.exe to run in 32-bit mode.

You can probably force the exe to always run 32bit with a few SDK tools, but it does require a little work.

The easy answer is to launch from a 32bit process (eg. use `%SystemRoot%\SYSWOW64\cmd.exe` to launch).

The more complex is to check what kind of exe it is, then modify it yourself. Background here is to understand that compiled code from languages that directly work with the Windows APIs are created as 32bit or 64bit at compile time by the developer. This cannot then be changed without going back to the source code.

However increasingly applications are written via a virtualisation layer that makes writing applications easier. There are two common ones: .NET and Java. I'm not sure about Java except knowing that forcing the right Java runtime install with solve the problem.

For .NET you can use SDK tools to:

Validate that the application is "AnyCPU": corflags myExe.exe. Using a utility from the .NET SDK to read the headers of a .NET assembly, for an exe will return something like:
```
Version   : v4.0.30319
CLR Header: 2.5
PE        : PE32
CorFlags  : 0x20003
ILONLY    : 1
32BITREQ  : 0
32BITPREF : 1
Signed    : 0
```
the `32BITREQ` tells me this is AnyCPU because 32bit is not required.

Use corflags with its `/32BITREQ+` option to modify the exe to be 32bit only.
Answer from Richard on superuser.com